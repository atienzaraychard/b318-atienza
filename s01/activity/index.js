/* 
Create a new class called Contractor
A contractor has the following properties: name, email, contactNo
A contractor also has a method called getContractorDetails and displays the name, email, and contactNo of the contractor
*/

class Contractor{
    constructor(name, email, contactNo){

        this.name = name;
        this.email = email;
        this.contactNo = contactNo;
    }

    getContractorDetails(){
        return `Name: ${this.name} \nEmail: ${this.email} \nContact No: ${this.contactNo}`
    }
}

let contractor1 = new Contractor("Ultra Manpower Services", "ultra@manpower.com", "09167890123")
console.log(contractor1.getContractorDetails());

/* 
Create a new class called Subcontractor that inherits the Contractor
A subcontractor has an additional property called specializations which is an array of specializations as strings
A subcontractor has a method called getSubConDetails that displays all the information of the subcontractor

*/

class Subcontractor extends Contractor {
    constructor(name, email, contactNo, specializations) {
        super(name, email, contactNo);
        this.specializations = specializations || [];
    }
    

    getSubConDetails() {
        console.log(`${this.getContractorDetails()} \n${this.specializations.join(", ")}`);
        return this;
    }
}

let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "09151234567", ["garden", "industrial"]);
let subCont2 = new Subcontractor("LitC Corp", "litc@mail.com", "091512456789", ["schools", "hospitals", "bakeries", "libraries"]);

console.log(subCont1.getSubConDetails());
console.log(subCont2.getSubConDetails());