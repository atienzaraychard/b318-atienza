/* 
1. Convert the RegularShape class from s02 - Polymorphism to an abstract class and add the necessary error messages. 
Modify the Square subclass from the previous session as needed.
*/

class RegularShape {
    constructor() {
    
        if(this.getPerimeter === undefined){
            throw new Error(
                "Class must implement getPerimeter() method."
            );    
        }
    }  
}

class Square extends RegularShape {
    constructor(noSides, length) {
        super()
        this.noSides = noSides;
        this.length = length;
    }
  
    getPerimeter() {
      return `The perimeter of the square is ${this.noSides * this.length}.`;
    }
  
    getArea() {
      return `The area of the square is ${this.length * this.length}.`;
    }
}

const shape1 = new Square(4, 16);
console.log(shape1.getPerimeter());
console.log(shape1.getArea());



/* 
Abstract classes may contain properties in their constructors. This is specially handy in terms of code reusability: properties common across all subclasses may instead be defined within their base class. 
Define a base class named Food which will have the properties name and price. It will require a method named getName from all its instances.

Define a subclass of Food named Vegetable which will add a property named breed to those already defined in its base class (Food). It will implement the getName method such that it returns the statement "<Name> is of <breed> variety and is priced at <price> pesos."
*/

class Food{
    constructor(name, price) {
        this.name = name;
        this.price = price

            if(this.getName === undefined){
            throw new Error(
                "Class must implement getName() method."
            );    
        }
    }

}

class Vegetable extends Food{
    constructor(name, breed, price){
        super(name, price)
        this.breed = breed;
    }

    getName(){
      return  `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`
    }
}

const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName());



/* 
3.    Convert the Equipment base class from s02 - Polymorphism into an abstract class with properties equipmentType and model, as well as requiring the implementation of a method named printInfo across all its instances.
Modify the Bulldozer and TowerCrane sub classes from the previous session as needed.
*/

class Equipment {
    constructor(equipmentType){
        this.equipmentType = equipmentType;

        if(this.printInfo === undefined){
            throw new Error(
                "Class must implement printInfo() method."
            );    
        }
    } 
}

class Bulldozer extends Equipment {
    constructor(equipmentType, model,bladeType ){
        super(equipmentType);
        this.model = model;
        this.bladeType = bladeType; 
    }
    printInfo(){
        return `Info: ${this.equipmentType} \nThe ${this.equipmentType} ${this.model} has a ${this.bladeType} blade`
    }
}

const bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

class TowerCrane extends Equipment {
    constructor(equipmentType, model, hookRadius,maxCapacity  ){
        super(equipmentType);
        this.model = model;
        this.hookRadius = hookRadius; 
        this.maxCapacity = maxCapacity; 
    }
    printInfo(){
        return `Info: ${this.equipmentType} \nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`       
    }
}

const towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towercrane1.printInfo())