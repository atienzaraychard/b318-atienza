class Request {
    constructor(requesterEmail, content, dateRequested) {
        this.requesterEmail = requesterEmail;
        this.content = content;
        this.dateRequested = dateRequested;
    }
}

class Person {
    #firstName;
    #lastName;
    #email;
    #department;

    constructor(firstName, lastName, email, department) {
        this.#firstName = firstName;
        this.#lastName = lastName;
        this.#email = email;
        this.#department = department;

        if (this.constructor === Person) {
            throw new Error("You are not allowed to generate object in Person class");
        }
    }

    getFirstName() {
        return `First Name: ${this.#firstName}`;
    }

    getLastName() {
        return `Last Name: ${this.#lastName}`;
    }

    getEmail() {
        return `Email: ${this.#email}`;
    }

    getDepartment() {
        return `Department: ${this.#department}`;
    }

    getFullName() {
        return `${this.#firstName} ${this.#lastName}`;
    }

    setFirstName(firstName) {
        this.#firstName = firstName;
    }

    setLastName(lastName) {
        this.#lastName = lastName;
    }

    setEmail(email) {
        this.#email = email;
    }

    setDepartment(department) {
        this.#department = department;
    }

    login() {
        return `${this.#firstName} ${this.#lastName} has logged in`;
    }

    logout() {
        return `${this.#firstName} ${this.#lastName} has logged out`;
    }
}

class Employee extends Person {
    #isActive;
    #requests;

    constructor(firstName, lastName, email, department, isActive = true, requests = []) {
        super(firstName, lastName, email, department);
        this.#isActive = isActive;
        this.#requests = requests;
    }

    getIsActive() {
        return `IsActive: ${this.#isActive}`;
    }

    getRequests() {
        return this.#requests;
    }

    setIsActive(isActive) {
        this.#isActive = isActive;
    }

    addRequest(content, dateRequested) {
        console.log(`Adding request for ${this.getEmail()}: ${content}`);
        const request = new Request(this.getEmail(), content, new Date(dateRequested));
        this.#requests.push(request);
        console.log(`Requests for ${this.getEmail()}: ${JSON.stringify(this.#requests)}`);
    }
}

class TeamLead extends Person {
    #isActive;
    #members;
    #requests;

    constructor(firstName, lastName, email, department, isActive = true, members = []) {
        super(firstName, lastName, email, department);
        this.#isActive = isActive;
        this.#members = members;
        this.#requests = [];
    }

    getIsActive() {
        return `IsActive: ${this.#isActive}`;
    }

    getMembers() {
        return this.#members;
    }

    getMemberRequests() {
        return this.#requests;
    }

    setIsActive(isActive) {
        this.#isActive = isActive;
    }

    setMembers(members) {
        this.#members = members;
    }

    addMember(employee) {
        if (employee instanceof Employee && !this.#members.includes(employee)) {
            this.#members.push(employee);
        } else {
            throw new Error("Invalid argument: expected an Employee object");
        }
    }

    checkRequests(email) {
        const requests = [];
        console.log(`Checking requests for email: ${email}`);
        if (this.getEmail() === email) {
            requests.push(...this.#requests);
        } else {
            for (const member of this.#members) {
                console.log(`Member email: ${member.getEmail()}`);
                if (member.getEmail() === email) {
                    requests.push(...member.getRequests());
                }
            }
        }
        console.log(`Requests for ${this.getEmail()}: ${JSON.stringify(this.#requests)}`);
    
        return requests;
    }
}

class Admin extends Person {
    #teamLeads;

    constructor(firstName, lastName, email, department, teamLeads = []) {
        super(firstName, lastName, email, department);
        this.#teamLeads = teamLeads;
    }

    getTeamLeads() {
        return this.#teamLeads;
    }

    setTeamLeads(teamLeads) {
        this.#teamLeads = teamLeads;
    }

    addTeamLead(teamLead) {
        if (teamLead instanceof TeamLead && !this.#teamLeads.includes(teamLead)) {
            this.#teamLeads.push(teamLead);
        } else {
            throw new Error("Invalid argument: expected a TeamLead object");
        }
    }

    findEmployeeByEmail(email) {
        for (const teamLead of this.#teamLeads) {
            for (const member of teamLead.getMembers()) {
                if (member.getEmail() === email) {
                    return member;
                }
            }
        }
        return null;
    }

deactivateTeam(teamLeadEmail) {
    let found = false;
    for (const teamLead of this.#teamLeads) {
        if (teamLead.getEmail() === teamLeadEmail) {
            found = true;
            teamLead.setIsActive(false);
            for (const member of teamLead.getMembers()) {
                member.setIsActive(false);
            }
            break;
        }
    }
}
}

const john = new Employee("John", "Doe", "john.doe@mail.com", "Engineering");
console.log(john.getFullName());
console.log(john.getEmail());
console.log(john.getDepartment());
/* john.setIsActive(true);
console.log(john.getIsActive());
john.setIsActive(false);
console.log(john.getIsActive()); */
john.addRequest("I need a new laptop", "2022-06-14");
console.log(john.getRequests());

const jane = new TeamLead("Jane", "Doe", "jane.doe@mail.com", "Engineering");
console.log(jane.getFullName());
console.log(jane.getEmail());
console.log(jane.getDepartment());
/* jane.setIsActive(true);
console.log(jane.getIsActive());
jane.setIsActive(false);
console.log(jane.getIsActive()); */
jane.addMember(john);
console.log(jane.getMembers());
console.log(`Members: ${jane.getMembers().map(member => member.getFullName()).join(", ")}`);

john.addRequest("I need a new monitor", "2022-06-15");
jane.checkRequests("john.doe@mail.com")

const admin = new Admin("Admin", "Admin", "admin@mail.com", "HR");
admin.addTeamLead(jane);
console.log(`Team Leads: ${admin.getTeamLeads().map(teamLead => teamLead.getFullName()).join(", ")}`);

admin.deactivateTeam("jane.doe@mail.com");
console.log(jane.getIsActive());
