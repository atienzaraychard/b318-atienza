/* 
S04 Activity Instructions:

1. Modify the RegularShape base class and the Triangle and Square subclasses from the previous session to encapsulate all their properties.
*/

class RegularShape {
    constructor() {

        if(this.constructor === RegularShape){
			throw new Error(
				"You are not allowed to generate object in RegularShape class"
			);
			
		}
    
        if(this.getPerimeter === undefined){
            throw new Error(
                "Class must implement getPerimeter() method."
            );    
        }

        if(this.getArea === undefined){
            throw new Error(
                "Class must implement getArea() method."
            );    
        }
    }  
}

class Square extends RegularShape {
    #noSides
    #length
    constructor(noSides, length) {
        super()
        this.#noSides = noSides;
        this.#length = length;
    }
  
    // getter methods
    getPerimeter() {
      return `The perimeter of the square is ${this.#noSides * this.#length}.`;
    }
  
    getArea() {
      return `The area of the square is ${this.#length * this.#length}.`;
    }

    getLength() {
        return `${this.#length}`;
      }

    // setter methods 
	setNoSides(noSides){
		this.#noSides = noSides;
	}

    setLength(length){
		this.#length = length;
	}
}

class Triangle extends RegularShape {
    #noSides
    #length
    constructor(noSides, length) {
        super()
        this.#noSides = noSides;
        this.#length = length;
    }
  
    // getter methods
    getPerimeter() {
      return `The perimeter is ${this.#noSides * this.#length}.`;
    }
  
    getArea() {
      return `The area is ${(Math.sqrt(this.#noSides) / 4) * this.#length * this.#length}.`;
    }

    getLength() {
        return `${this.#length}`;
      }

    // setter methods 
	setNoSides(noSides){
		this.#noSides = noSides;
	}

    setLength(length){
		this.#length = length;
	}
  }
  
const square1 = new Square();
square1.setNoSides(4);
square1.setLength(65);
console.log(square1.getLength());
console.log(square1.getPerimeter());

const triangle1 = new Triangle();
triangle1.setNoSides(3);
triangle1.setLength(15);
console.log(triangle1.getLength());
console.log(triangle1.getArea());


/* 
2. Create a base class called User with an empty constructor and is an abstract class that **requires** the following methods: 
login
register
logout
*/

class User {
    constructor() {

        if(this.constructor === User){
			throw new Error(
				"You are not allowed to generate object in User class"
			);
			
		}
    
        if(this.login === undefined){
            throw new Error(
                "Method must implement login method."
            );    
        }

        if(this.register === undefined){
            throw new Error(
                "Method must implement register method."
            );    
        }

        if(this.logout === undefined){
            throw new Error(
                "Method must implement logout method."
            );    
        }
    }  
}

class RegularUser extends User{
    #name
    #email
    #password
    constructor(name, email, password){
        super()
        this.#name = name;
        this.#email = email;
        this.#password = password;
    }

    // Getters
    getName(){
        return this.#name
    }

    getEmail(){
        return this.#email
    }

    getPassword(){
        return this.#password
    }

    login() {
        return `${this.#name} has logged in.`
    }

    register() {
        return `${this.#name} has registered.`
    }

    logout() {
        return `${this.#name} has logged out.`
    }

    browseJobs() {
        return `There are 10 jobs found`
    }

    setName(name){
        this.#name = name;
    }

    setEmail(email){
        this.#email = email;
    }

    setPassword(password){
        this.#password = password;
    }
}

const regUser1 = new RegularUser();
regUser1.setName("Dan");
regUser1.setEmail("dan@mail.com");
regUser1.setPassword("Dan12345");
console.log(regUser1.register());
console.log(regUser1.login());
console.log(regUser1.browseJobs());
console.log(regUser1.logout());

/* 
Create another subclass of User called Admin with the following properties:
name
email
password
hasAdminExpired
and methods:
login - returns "Admin <name> has logged in."
register - returns "Admin <name> has registered"
logout - returns "Admin <name> has logged out"
postJob - returns "Job posting added to site"
*/

class Admin  extends User{
    #name
    #email
    #password
    #hasAdminExpired
    constructor(name, email, password, hasAdminExpired){
        super()
        this.#name = name;
        this.#email = email;
        this.#password = password;
        this.#hasAdminExpired = hasAdminExpired;
    }

    // Getters
    getName(){
        return this.#name
    }

    getEmail(){
        return this.#email
    }

    getPassword(){
        return this.#password
    }

    getHasAdminExpired(){
        return this.#hasAdminExpired
    }

    login() {
        return `Admin ${this.#name} has logged in.`
    }

    register() {
        return `Admin ${this.#name} has registered.`
    }

    logout() {
        return `Admin ${this.#name} has logged out.`
    }

    postJob() {
        return `Job added to site`
    }

    setName(name){
        this.#name = name;
    }

    setEmail(email){
        this.#email = email;
    }

    setPassword(password){
        this.#password = password;
    }

    sethasAdminExpired(hasAdminExpired){
        this.#hasAdminExpired = hasAdminExpired;
    }
}

const admin = new Admin();
admin.setName("Joe");
admin.setEmail("admin_joe@mail.com");
admin.setPassword("joel12345");
admin.sethasAdminExpired(false);
console.log(admin.register());
console.log(admin.login());
console.log(admin.getHasAdminExpired());
console.log(admin.postJob());
console.log(admin.logout());