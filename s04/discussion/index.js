class Person{
    constructor(){
        if(this.constructor === Person){
            throw new Error(
                "Object cannot be created from an abstract class Person"
            );
        }

        if(this.getFullName === undefined){
            throw new Error(
                "Class must implement getFullName() method."
            );    
        }
    }
}

class Employee extends Person {

    //Encapsulation aided by private fields (#), ensures data protection.
    //Setters and getter provide controlled access to encapsulated data.

    //Prive Fields
    #firstName;
    #lastName;
    #employeeID;
    constructor(firstName, lastName, employeeID){
        super()
        this.#firstName = firstName;
        this.#lastName = lastName;
        this.#employeeID = employeeID;
    }

    // Getters
    getFirstName(){
        return `First Name: ${this.#firstName}`
    }

    getLastName(){
        return `Last Name: ${this.#lastName}`
    }

    getEmployeeId(){
        return this.#employeeID;
    }

    getFullName(){
        return `${this.#firstName} ${this.#lastName} has employee ID of ${this.#employeeID}`
    }

    // setter metods
    setFirstname(firstName){
        this.#firstName = firstName;
    }

    setLastName(lastName){
        this.#lastName = lastName;
    }

    setEmployeeId(employeeID){
        this.#employeeID = employeeID;
    }
}

const employeeA = new Employee('John', 'Smith', 'EM=001');
// Direct access with the field/property firstName will return undefine because the property is in a private.
// console.log(employeeA.firstName);

// We could directly change the value of the property firstName because the propert is private.

//employeeA.firstName = 'David';

console.log(employeeA.getFirstName())
employeeA.setFirstname('David');
console.log(employeeA.getFirstName())
console.log(employeeA.getFullName())

// employeeB.firstName = "Jill";
// employeeB.lastName = "Hill";
// employeeB.employeeID = "EM-002"

const employeeB = new Employee();
console.log(employeeB.getFullName())
employeeB.setFirstname("Jill")
employeeB.setLastName("Hill")
employeeB.setEmployeeId("EM-002")

console.log(employeeB.getFullName())