/* 
Activity 1
1.Create a base class called Equipment with the following property and method:
property: <equipmentType>
method: printInfo() that returns "Info: <equipmentType>"

*/

class Equipment {
    constructor(equipmentType){
        this.equipmentType = equipmentType;
    }

    printInfo(){
        return `Info: ${this.equipmentType}`
    }
}


/* 
2. Create a class called Bulldozer from Equipment with additional properties:
properties: <model>, <bladeType>
method: printInfo that adds "The bulldozer <model> has a <bladeType> blade" to the inherited printInfo method
*/
class Bulldozer extends Equipment {
    constructor(equipmentType, model,bladeType ){
        super(equipmentType);
        this.model = model;
        this.bladeType = bladeType; 
    }
    printInfo(){
        return `Info: ${this.equipmentType} \nThe ${this.equipmentType} ${this.model} has a ${this.bladeType} blade`
    }
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());

/* 
3. Create a class called TowerCrane from Equipment with additional properties:
properties: <model>, <hookRadius>, <maxCapacity>
method: printInfo that adds "The tower crane <model> has <hookRadius> cm hook radius and <maxCapacity> kg max capacity" to the inherited printInfo method
*/

class TowerCrane extends Equipment {
    constructor(equipmentType, model, hookRadius,maxCapacity  ){
        super(equipmentType);
        this.model = model;
        this.hookRadius = hookRadius; 
        this.maxCapacity = maxCapacity; 
    }
    printInfo(){
        return `Info: ${this.equipmentType} \nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`       
    }
}

let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towercrane1.printInfo())

/* 
4. Create a class called BackLoader from Equipment with additional properties:
properties: <model>, <type>, <tippingLoad>
method: printInfo that adds "The loader <model> is a <type> loader and has a tipping load of <tippingLoad> lbs." to the inherited printInfo method
*/

class BackLoader extends Equipment{
    constructor(equipmentType, model, type, tippingLoad ){
        super(equipmentType);
        this.model = model;
        this.type = type; 
        this.tippingLoad = tippingLoad; 
    }
    printInfo(){
        return `Info: ${this.equipmentType} \nThe loader ${this.model} is a ${this.type} and has a tipping load of ${this.tippingLoad} lbs`
    }
}

let backloader1 = new BackLoader("backloader", "Turtle", "hrdraulic", 1500);
console.log(backloader1.printInfo());


/* 
Activity 2

5. Create a class called RegularShape with the following properties and methods:
properties: <noSides>, <length>
methods: 
perimeter - that returns "the perimeter is "
area - that returns "the area is "
*/

class RegularShape {
    constructor(noSides, length) {
      this.noSides = noSides;
      this.length = length;
    }
  
    getPerimeter() {    
      return `The perimeter is.`;
    }
  
    getArea() {       
        return `The area is.`;
      }
}

/* 
6. Create a class called Triangle from RegularShape and override the methods to compute for the perimeter and area. Take note that since we only have 1 property for length, this triangle will be equilateral.
*/

class Triangle extends RegularShape {
    constructor(noSides, length) {
      super(noSides, length);
    }
  
    getPerimeter() {
      return `The perimeter is ${this.noSides * this.length}.`;
    }
  
    getArea() {
      return `The area is ${(Math.sqrt(this.noSides) / 4) * this.length * this.length}.`;
    }
  }
  
let triangle1 = new Triangle(3, 10);
console.log(triangle1.getPerimeter());
console.log(triangle1.getArea());

/* 
7. Create a class called Square from RegularShape and override the methods to compute for the perimeter and area
Test Input:
*/

class Square extends RegularShape {
    constructor(noSides, length) {
        super(noSides, length);
    }
  
    getPerimeter() {
      return `The perimeter is ${this.noSides * this.length}.`;
    }
  
    getArea() {
      return `The area is ${this.length * this.length}.`;
    }
}

let square1 = new Square(4, 12);
console.log(square1.getPerimeter());
console.log(square1.getArea());

